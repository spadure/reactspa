
import {connect} from "react-redux";
import {Sum} from "../components/sumComponent.jsx";


const mapStateToProps = (state) =>({
    _getData: state.number
});

export const Sum_component = connect(mapStateToProps)(Sum);
