
import {connect} from "react-redux";
import {randomGenerate} from "../actions/actions.jsx";
import {Rand} from "../components/randComponent.jsx";

const mapStateToProps = state => ({
    _data: state.number
});

const mapDispatchToProps = dispatch => ({
    _generate: (v) => {dispatch(randomGenerate(v))}
});

export let Generator = connect(mapStateToProps, mapDispatchToProps)(Rand);