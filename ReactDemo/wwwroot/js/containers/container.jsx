﻿import { connect } from "react-redux";
import { Stuff } from "../components/red_ux.jsx";
import {addText} from "../actions/actions.jsx";


const mapStateToProps = (state) =>
    ({
        _all:  state.allActions,
        _count: state.number
    });

const mapDispatchToProps = dispatch =>
    ({
        _add: (m) => dispatch(addText(m))
    });



export const Connect_component = connect(mapStateToProps, mapDispatchToProps)(Stuff);