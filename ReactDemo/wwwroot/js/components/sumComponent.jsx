
import React from "react";

export class Sum extends React.Component{
    constructor(props){
        super(props);
    }

    getSum(array){
        return array.reduce(function (a, b) {
            return a + b;
        }, 0);
    }

    render(){
        return(
            <div>
                <div className="container">
                    <div className="col-lg-2">
                        <input className="form-control" ref="rand" value={this.getSum(this.props._getData)}/>
                    </div>
                </div>
            </div>
        );
    }
}
