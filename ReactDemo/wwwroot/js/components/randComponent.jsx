﻿
import React from "react";


export class Rand extends React.Component{
    constructor(props){
        super(props);
    }

    generateRandom(){
        let rand = Math.floor((Math.random() * 100) + 1);
        this.props._generate(rand);
    }
    onClickGenButton(e){
        setInterval(this.generateRandom.bind(this), 1000);
    }

    render(){

        return(
            <div className="container">
                <div className="col-lg-2">
                    <input className="form-control" ref="rand" value={this.props._data[this.props._data.length - 1]}/>
                    <button className="bg-primary" onClick={this.onClickGenButton.bind(this)}> Generate </button>

                </div>
            </div>
        );
    }
}

