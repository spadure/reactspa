
import React from "react";
import {connect} from "react-redux"
import {AreaChart} from "react-easy-chart";


class ToolTip extends React.Component{
    constructor(props){
        super(props);
    }

    render() {
        return(
            <div>
                <b>{this.props.text}</b> : {this.props.text1}
            </div>
        );
    }
}

class Chart_ extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            showToolTip: false,
            top: "",
            left: "",
            y: "",
            x: "",
            componentWidth: 900
        }
    }


    returnData(data, _length){
        if(data.length > _length) {
            return data.slice(Math.max(data.length - _length, 1));
        } else return data;
    }

///////////////////DATA POINT EVENTS//////////////////////////

    mouseOverHandler(d, e) {
        this.setState({
            showToolTip: true,
            top: `${e.screenY - 10}px`,
            left: `${e.screenX + 10}px`,
            y: d.y,
            x: d.x
        });
    }

    mouseMoveHandler(e) {
        if (this.state.showToolTip) {
            this.setState({ top: `${e.y - 10}px`, left: `${e.x + 10}px` });
        }
    }

    mouseOutHandler() {
        this.setState({ showToolTip: false });
    }
///////////////DATA POINT EVENTS///////////////////////

    render(){

        return(
            <div>
                <div className="container" ref="div">
                                    <AreaChart
                        xType={"text"}
                        axes={(this.state.componentWidth) > 400 ? true : false}
                        yDomainRange={[0, 100]}
                        // dataPoints
                        grid
                        mouseOverHandler={this.mouseOverHandler.bind(this)}
                        mouseOutHandler={this.mouseOutHandler.bind(this)}
                        mouseMoveHandler={this.mouseMoveHandler.bind(this)}
                        verticalGrid
                        margin={{top: 10, right: 10, bottom: 50, left: 50}}
                        width={this.state.componentWidth}
                        height={this.state.componentWidth / 2}
                        areaColors={["orange"]}
                        interpolate={'cardinal'}

                        data={[
                            [
                                { x: "January", y: this.returnData(this.props.chart_data, 12)[0] },
                                { x: "February", y: this.returnData(this.props.chart_data, 12)[1] },
                                { x: "March", y: this.returnData(this.props.chart_data, 12)[2] },
                                { x: "April", y: this.returnData(this.props.chart_data, 12)[3] },
                                { x: "May", y: this.returnData(this.props.chart_data, 12)[4] },
                                { x: "June", y: this.returnData(this.props.chart_data, 12)[5]},
                                { x: "July", y: this.returnData(this.props.chart_data, 12)[6] },
                                { x: "August", y: this.returnData(this.props.chart_data, 12)[7] },
                                { x: "September", y: this.returnData(this.props.chart_data, 12)[8] },
                                { x: "October", y: this.returnData(this.props.chart_data, 12)[9] },
                                { x: "November", y: this.returnData(this.props.chart_data, 12)[10] },
                                { x: "December", y: this.returnData(this.props.chart_data, 12)[11] }
                            ]
                        ]}
                    />
                    <ToolTip text={this.state.x} text1={this.state.y}/>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) =>({
   chart_data: state.number
});

export const Chart_component = connect(mapStateToProps)(Chart_);