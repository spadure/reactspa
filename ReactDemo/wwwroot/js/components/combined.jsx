
import React from "react";
import {Connect_component} from "../containers/container.jsx";
import {Generator} from "../containers/generateContainer.jsx";
import {Sum_component} from "../containers/sumContainer.jsx";
import {Chart_component} from "../components/chart.jsx";

export class App extends React.Component{
    constructor(props){
        super(props);
    }
    render(){
        console.log("generator");
        return(
            <div>
                <Connect_component/>
                <Generator/>
                <Sum_component/>
                <Chart_component/>
            </div>
        );
    }
}

