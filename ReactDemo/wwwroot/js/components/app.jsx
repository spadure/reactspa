﻿import React from "react";
import { render } from "react-dom";
import { createStore} from "redux";
import { Provider } from "react-redux";
import  {reducer}  from "../reducers/reducers.jsx";
import { _ReducerFunction } from "../reducers/reducers.jsx";
import { _ReducerGenerator } from "../reducers/reducers.jsx";
import {Connect_component} from "../containers/container.jsx";
import {Generator} from "../containers/generateContainer.jsx";



import {App} from "./combined.jsx";
const initialState = {
     allActions: [{ id: 0, text: "One"}, {id: 1, text: "Two"}],
    number: []

}


const store = createStore(reducer,  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());




render(<Provider store={store}>
        <App/>
       </Provider>,
    document.getElementById("content"));

