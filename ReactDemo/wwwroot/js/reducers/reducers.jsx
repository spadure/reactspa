﻿
"use strict"

import { combineReducers } from "redux";


 function _ReducerFunction(state = [], action) {
    switch (action.type) {
        case "ADD_TEXT":
            return [...state, action.allActions];
        default:
            return state;
    }
}

 function _ReducerGenerator(state = [0, 0,0,0,0,0,0,0,0,0,0,0,], action) {
    switch (action.type){
        case "RANDOM_GENERATE":
            return [...state, action.number];

        default:
            return state;
    }
}

export const reducer = combineReducers({
    allActions: _ReducerFunction,
    number: _ReducerGenerator
})


