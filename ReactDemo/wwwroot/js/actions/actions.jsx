﻿

let id = 2;

export function addText(text) {
    return {
        type: "ADD_TEXT",
        allActions: {
            id: id++,
            text: text
        }   
    }
}

export function randomGenerate(value) {
    return{
        type: "RANDOM_GENERATE",
        number: value
    }
}

