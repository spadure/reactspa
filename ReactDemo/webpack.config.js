﻿"use strict";

module.exports = {
    entry: "./wwwroot/js/components/app.jsx",
    output: {
        filename: "./wwwroot/js/bundle.js"
    },
    module: {
        loaders: [
            {
                test: /\.jsx$/,
                loader: "babel-loader",
                exclude: /node_modules/,
                query: {
                    presets: ["es2015", "react"]
                }
            }
        ]
    },
    watch: true,
    devtool: "source-map",
    devServer: {
        host: "localhost",
        port:4040
    }
}